import datetime
import re
import CinemaModel as Model
import CinemaView as View

class CinemaController:
    def __init__(self, model, view):
        self.model=model
        self.view=view

    def __del__(self):
        if(self.model.is_connected()):
            self.model.disconnect()

    def start_work(self):
        self.model.connect()

    def stop_work(self):
        self.model.disconnect()

    def intro_generator(self):
        customer_amt, ticket_amt=("","")
        while(True):
            print("So, how many customers will come in next month?")
            customer_amt=input()
            if(not customer_amt.isprintable() or len(customer_amt)==0):
                self.view.no_input()
            elif(customer_amt.isdigit()==True):
                break
            else:
                self.view.no_number()
        while(True):
            print("And how many tickets will customers buy in next month?")
            ticket_amt=input()
            if(not ticket_amt.isprintable() or len(ticket_amt)==0):
                self.view.no_input()
            elif(ticket_amt.isdigit()==True):
                break
            else:
                self.view.no_number()
        self.model.generate(int(customer_amt), int(ticket_amt))

    def create_customer(self):
        new_name=""
        new_phone=""
        new_DOB=""
        phone_format=re.compile("^\+38\(0[0-9][0-9]\)[0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$")
        date_format=re.compile("[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]")
        print("Before we start, our cinema has a rule that customers under 10 years cannot be added to our database")
        print("So we'll ask you now before it's too late: is your customer old enough? [y/n]")
        check=input()
        if(check=='y'):
            print("Then let's begin")
            while(True):
                print("Enter customer's new name")
                new_name = input()
                if(len(new_name)==0 and not new_name.isprintable()):
                    self.view.no_input()
                else:
                    break
            while(True):
                print("Enter customer's new phone in format +38(0XX)XXX-XX-XX")
                new_phone = input()
                if(len(new_phone)==0):
                    self.view.no_input()
                elif(not phone_format.match(new_phone)):
                    self.view.wrong_format()
                elif(not self.model.find_customer(new_phone)==None):
                    self.view.duplicate_value()
                else:
                    break
            while(True):
                print("Enter customer's date of birth in format YYYY-MM-DD")
                new_DOB=input()
                today = datetime.date.today()
                if(len(new_DOB)==0):
                    self.view.no_input()
                elif(not date_format.match(new_DOB)):
                    self.view.wrong_format()
                year,month,day=new_DOB.split('-')
                try:
                    dob=datetime.date(int(year), int(month), int(day))
                    if(today.year-int(year)-((today.month, today.day)<(int(month), int(day)))<10):
                        print("You lied to me! Your customer's too young! Try again")
                    elif(datetime.date.now()<dob):
                        print("No customers from future, sorry. Try again")
                    else:
                        break
                except ValueError:
                    self.view.wrong_date()
            new_ID=self.model.create_customer(new_name, new_phone, new_DOB)
            if(new_ID!=-1):
                self.view.success("Customer", "created")
                self.view.show_customer(new_ID[0], new_name, new_DOB, new_phone)
            else:
                self.view.fail()
        elif(check=='n'):
            print("So be it")
            return
        else:
            print("I can't understand you, so I'll count that as no")
            return

    def update_customer(self):
        phone_format=re.compile("^\+38\(0[0-9][0-9]\)[0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$")
        print("Enter customer's ID or phone number")
        data=input()
        customer=None
        if(data.isdigit() or phone_format.match(data)):
            customer=self.model.find_customer(data)
        else:
            print("That's neither valid phone nor valid ID. We're done here")
            return
        if(not customer==None):
            print("We found your customer!")
            new_name=""
            new_phone=""
            new_DOB=""
            date_format=re.compile("[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]")
            print("Before we start, our cinema has a rule that customers under 10 years cannot be added to our database")
            print("So we'll ask you now before it's too late: is your customer old enough? [y/n]")
            check=input()
            if(check=='y'):
                print("Then let's begin")
                while(True):
                    print("Enter customer's new name (or skip if you don't want to change it)")
                    new_name = input()
                    if(len(new_name)==0 and not new_name.isprintable()):
                        self.view.no_input()
                    elif(new_name=="skip"):
                        new_name=customer[1]
                        break
                    else:
                        break
                while(True):
                    print("Enter customer's new phone in format +38(0XX)XXX-XX-XX (or skip if you don't want to change it)")
                    new_phone = input()
                    if(len(new_phone)==0):
                        self.view.no_input()
                    elif(new_phone=="skip"):
                        new_phone=customer[3]
                        break
                    elif(not phone_format.match(new_phone)):
                        self.view.wrong_format()
                    elif(not self.model.find_customer_by_phone(new_phone)==None):
                        self.view.duplicate_value()
                    else:
                        break
                while(True):
                    print("Enter customer's new date of birth in format YYYY-MM-DD")
                    new_DOB=input()
                    today = datetime.date.today()
                    if(len(new_DOB)==0):
                        self.view.no_input()
                    elif(new_DOB=="skip"):
                        new_DOB=customer[2]
                        break
                    elif(not date_format.match(new_DOB)):
                        self.view.wrong_format()
                    year,month,day=new_DOB.split('-')
                    try:
                        datetime.date(int(year), int(month), int(day))
                        if(today.year-int(year)-((today.month, today.day)<(int(month), int(day)))<10):
                            print("You lied to me! Your customer's suddenly too young! Try again")
                        else:
                            break
                    except ValueError:
                        self.view.wrong_date()
                if(self.model.update_customer(customer[0], new_name, new_phone, new_DOB)!=-1):
                    self.view.success("Customer", "updated")
                    self.view.show_customer(customer[0], new_name, new_DOB, new_phone)
                else:
                    self.view.fail()
            elif(check=='n'):
                print("So be it")
                return
            else:
                print("I can't understand you, so I'll count that as no")
                return
        else:
            self.view.not_found("Customer")

    def delete_customer(self):
        phone_format=re.compile("^\+38\(0[0-9][0-9]\)[0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$")
        print("Enter customer's ID or phone number")
        data=input()
        customer=None
        if(data.isdigit() or phone_format.match(data)):
            customer=self.model.find_customer(data)
        else:
            print("That's neither valid phone nor valid ID. We're done here")
            return
        if(not customer==None):
            print("We found your customer!")
            print("Let me just check he has nothing to leave after self...")
            if(not self.model.check_customer_dependencies(customer)==None):
                print("Hold on a second. Customer has at least one ticket!")
                print("In order to delete customer we'll have to delete ALL the tickets they have")
                print("Do you want to erase all customer's data (including dependencies)? [y/n]")
                check=input()
                if(check=='y'):
                    print("I warned you")
                    if(self.model.delete_customer(customer[0])!=-1):
                        self.view.success("Customer", "deleted")
                    else:
                        self.view.fail()
                elif(check=='n'):
                    print("So be it")
                    return
                else:
                    print("I can't understand you, so I'll count that as no")
                    return
        else:
            self.view.not_found("Customer")

    def create_ticket(self):
        customer=None
        movie_id=""
        seance_id=""
        seat_row=""
        seat_col=""
        airday=""
        phone_format=re.compile("^\+38\(0[0-9][0-9]\)[0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$")
        date_format=re.compile("[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]")
        print("So, let's get started.")
        print("Firstly, enter customer's phone in format +38(0XX)XXX-XX-XX")
        new_phone = input()
        if(len(new_phone)==0):
            self.view.no_input()
            self.view.return_back()
            return
        elif(not phone_format.match(new_phone)):
            self.view.wrong_format()
            self.view.return_back()
            return
        customer=self.model.find_customer(new_phone)
        if(customer==None):
            self.view.not_found("Customer")
            self.view.return_back()
            return
        movies=self.model.get_movies(customer[0])
        self.view.show_movies(movies)
        while(True):
            print("So, which movie does he want to watch? Enter its ID")
            movie_id=input()
            if(not movie_id.isprintable() or len(movie_id)==0):
                self.view.no_input()
            elif(movie_id.isdigit()==True and len([item for item in movies if int(item[0]) == int(movie_id)])!=0):
                break
            else:
                self.view.no_number()
        seances=self.model.get_seances(int(movie_id))
        self.view.show_seances(seances)
        while(True):
            print("Please, enter the number of seance the customer wants to attend")
            seance_id=input()
            if(not seance_id.isprintable() or len(seance_id)==0):
                self.view.no_input()
            elif(seance_id.isdigit()==True and len([item for item in seances if int(item[0]) == int(seance_id)])!=0):
                break
            else:
                self.view.no_number()
        while(True):
            print("Enter the date when customer wants to attend cinema in format YYYY-MM-DD")
            print("Note: we sell tickets only starting from tomorrow")
            airday=input()
            today = datetime.date.today()
            if(len(airday)==0):
                self.view.no_input()
            elif(not date_format.match(airday)):
                self.view.wrong_format()
            else:
                year,month,day=airday.split('-')
                try:
                    cast_day=datetime.date(int(year), int(month), int(day))
                    if(datetime.date.today()>=cast_day):
                        print("We can't sell tickets for the past and today's casts, sorry. Try again")
                    else:
                        break
                except ValueError:
                    self.view.wrong_date()
        row_amount=int(self.model.get_row_amount(str([item for item in seances if int(item[0]) == int(seance_id)]).split(',')[1])[0])
        taken_seats=self.model.get_taken_seats(airday, seance_id)
        self.view.show_taken_seats(taken_seats)
        duplicate=True
        while(duplicate):
            print("Choose row from 1 to {} and seat from 1 to 15".format(row_amount))
            while(True):
                print("Let's start with row")
                seat_row=input()
                if(not seat_row.isprintable() or len(seat_row)==0):
                    self.view.no_input()
                elif(seat_row.isdigit()==True and int(seat_row)<=row_amount):
                    break
                else:
                    self.view.no_number()
            while(True):
                print("Now choose seat")
                seat_col=input()
                if(not seat_col.isprintable() or len(seat_col)==0):
                    self.view.no_input()
                elif(seat_col.isdigit()==True and int(seat_col)<=15):
                    break
                else:
                    self.view.no_number()
            for place in taken_seats:
                if(not int(place[0])==int(seat_row)):
                    continue
                seats=place[1].split(', ')
                if(seat_col in seats):
                    print("Don't you see that this seat is already taken? Choose another seat")
                    break
                else:
                    duplicate=False
                    break
        new_ID=self.model.create_ticket(customer[0], seance_id, seat_row, seat_col, airday)
        if(new_ID==-1):
            self.view.ticket_exists()
        else:
            self.view.success("Ticket", "created")
            print("It's ID is ", new_ID[0])

    def delete_ticket(self):
        print("Enter ticket's ID")
        data=input()
        ticket=None
        if(not data.isdigit()):
            print("That's not valid ID. We're done here")
            return
        ticket=self.model.find_ticket(data)
        if(not ticket==None):
            print("We found your ticket! Now let's get rid of it")
            if(self.model.delete_ticket(ticket[0])==True):
                self.view.success("Ticket", "deleted")
            else:
                self.view.fail()
        else:
            self.view.not_found("Ticket")

    def calculate_first_statistics(self):
        age_from=""
        age_to=""
        while(True):
            print("Enter lowest age (staring from 10)")
            age_from=input()
            if(not age_from.isdigit()):
                self.view.no_number()
            elif(len(age_from)==0):
                self.view.no_input()
            elif(int(age_from)<10):
                print("Our cinema doesn't have customers aged below 10, but we'll accept it")
                break
            else:
                break
        while(True):
            print("Enter highest age")
            age_to=input()
            if(not age_to.isdigit()):
                self.view.no_number()
            elif(len(age_to)==0):
                self.view.no_input()
            elif(int(age_to)<=int(age_from)):
                print("Highest age must be higher than lowest, right? You must've made a mistake")
            elif(int(age_to)>=80):
                print("We don't think people that old go to cinema, but we'll accept it")
                break
            else:
                break
        start=datetime.datetime.now()
        movies=self.model.get_first_statistics(age_from, age_to)
        finish=datetime.datetime.now()
        delta=(finish-start)
        self.view.print_first_stats(movies)
        self.view.show_time(delta.total_seconds()*1000)

    def calculate_second_statistics(self):
        movies=self.model.get_all_movies()
        self.view.show_movies(movies)
        while(True):
            print("Enter movie's ID, please")
            movie_id=input()
            if(not movie_id.isprintable() or len(movie_id)==0):
                self.view.no_input()
            elif(movie_id.isdigit()==True and len([item for item in movies if int(item[0]) == int(movie_id)])!=0):
                break
            else:
                self.view.no_number()
        start=datetime.datetime.now()
        result=self.model.get_second_statistics(movie_id)
        finish=datetime.datetime.now()
        delta=(finish-start)
        self.view.print_second_stats(result)
        self.view.show_time(delta.total_seconds()*1000)

    def calculate_third_statistics(self):
        print("So, how many VIP clients do you want to see? Enter number up to 15")
        while(True):
            amt=input()
            if(not amt.isprintable() or len(amt)==0):
                self.view.no_input()
            elif(amt.isdigit()==True and int(amt)>15):
                print("That's too much VIP clients. We can't get all of them at the same time")
            elif(amt.isdigit()==True and int(amt)<=15):
                break
            else:
                self.view.no_number()
        start=datetime.datetime.now()
        clients=self.model.get_third_statistics(amt)
        finish=datetime.datetime.now()
        delta=(finish-start)
        self.view.print_third_stats(clients)
