from os import system

class CinemaView:
    def intro(self):
        print("Now before we begin, we need to generate some tickets and customers in our database")
        print("Don't worry: there will be no leftovers from previous users")
        print("Only your personal customers and tickers")

    def no_number(self):
        print("That doesn't seem like an appropriate number. Try again")

    def menu(self):
        print("\t\tGreetings, our dear client!")
        print("\tHere's what we have on the menu:")
        print("create/customer - Yay, cinema has a new client!")
        print("update/customer - Got client's details wrong? No problem")
        print("delete/customer - Add this pesky client to cinema's black list")
        print("create/ticket - Yay, someone bought a ticket!")
        print("delete/ticket - What a pity someone changed their mind...")
        print("clear - Already full and want to start over? We'll clean everything up and bring you menu back!")
        print("quit - Don't forget to tip the waitress, okay?")
        print("And if you want some statistics, we have some of the freshest just for you!")
        print("Say ein to know what are the most popular movies for selected age gap.")
        print("Say zwei to know what is the most popular time for selected movie (and how much tickets were sold!)")
        print("Say drei to get to known with the VIP clients of our cinema (you get to choose how much you want to meet!")

    def show_customer(self, ID, name, DOB, phone):
        print("ID: ", ID)
        print("Name: ", name)
        print("DOB: ", DOB)
        print("Phone: ", phone)

    def show_movies(self, movies):
        print("Here are available movies:")
        for movie in movies:
            print("ID: ", movie[0])
            print("Name: ", movie[1])
            print("Length: ", movie[2])
            print("Age restriction: ", movie[3])

    def show_seances(self, seances):
        print("Here are the available seances for this movie:")
        for seance in seances:
            print("Seance №{}: at {} in hall №{}".format(seance[0], seance[3], seance[1]))

    def show_taken_seats(self, seats):
        print("Here are seats that are TAKEN:")
        for seat in seats:
            print("Row {0}: {1}".format(seat[0], seat[1]))

    def print_first_stats(self, movie_names):
        count=1
        print("And here we present you favorites of this age group:")
        for movie in movie_names:
            print("№{0}: {1}".format(count, movie[0]))
            count=count+1

    def print_second_stats(self, result):
        print("The most popular time of this movie is {0}. \
            \nWe sold whopping {1} tickets on that time!".format(result[0], int(result[1])))

    def print_third_stats(self, clients):
        count=1
        print("These are our VIP clients:")
        for client in clients:
            print("\t№", count)
            print("Name: ", client[0])
            print("Phone: ", client[1])
            print("Tickets bought recently: ", client[2])
            count=count+1

    def clear_screen(self):
        system('clear')
        self.menu()

    def invalid_input(self, command):
        print("Sorry, we don't have {} on the menu. Try something else".format(command))

    def no_input(self):
        print("I can't hear you. Could you repeat what you said?")

    def wrong_format(self):
        print("This is elfish for me. Make sure you follow the format")

    def wrong_date(self):
        print("This isn't real date! Try again")

    def duplicate_value(self):
        print("I'm sorry to inform you, but this field must be unique in database")
        print("Someone else already used this value, so you'll have to enter something else")

    def ticket_exists(self):
        print("I'm sorry to inform you, but someone already got this exact place at this exact time right before you")
        print("Here, have a balloon instead while you make up your mind")
        print('\n'.join([''.join([('Love'[(x-y) % len('Love')] if ((x*0.05)**2+(y*0.1)**2-1)**3-(x*0.05)**2*(y*0.1)**3 <= 0 else ' ') for x in range(-30, 30)]) for y in range(30, -30, -1)]))

    def success(self, table, command):
        print("{0} successfully {1}!".format(table, command))

    def fail(self):
        print("Oops, something went wrong. We're sorry to inform you that we can't fulfill your wish")

    def return_back(self):
        print("Returning back to main menu")

    def not_found(self, data):
        print("Error 404. {} not found".format(data))

    def show_time(self, time):
        print("And the query took only {} milliseconds to do!".format(time))