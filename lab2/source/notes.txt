
Some really good queries

NUMBER 1.
        Get random ID from halls table
select id from halls order by random() limit 1

NUMBER 2.
        Insert random seances (change limit to change amount)
insert into seances (hall_id, movie_id, times_id)
select t1.id, round(random()*7+0.5), t2.id from halls as t1, times as t2
where not exists (select 1 from seances as t0 where t1.id=t0.hall_id and t2.id=t0.times_id)
order by random() limit 100

NUMBER 3.
        I believe, we check how many seats are there in hall currently and what is the maximum number of seats in row
select seats.hall_id, halls.name, count(*), max(seats.seat_num) from
        seats, halls where seats.hall_id=halls.id group by seats.hall_id, halls.name

NUMBER 4.
        Generate seats for hall (we made seat amount in hall divisible by 15. If it's not, some seats might be omitted)
insert into seats (hall_id, row_num, seat_num)
select id, row_number, seat_number
from halls
cross join generate_series(1, seat_amount/15) as row_number, generate_series(1, 15) as seat_number
order by id, row_number, seat_number asc

NUMBER 5.
        Get customer's age for today
select date_part('year', now()) - date_part('year', dob) from customers

NUMBER 6.
        Get random date between next day and (almost) four weeks after next day (because random() cannot be 1)
select date(now() + interval '1 day' + random() * (interval '4 weeks')) as creation_date

NUMBER 7.
        Get movies that selected customer can watch
select * from movies where age_restriction<
        (select date_part('year', now()) - date_part('year', dob) from customers where id=58)

NUMBER 8.
        Checks whether selected customer can watch specified movie (need correction in case you want to use it)
SELECT "Movie"."Age_restriction"<(SELECT date_part('year',age("Date_of_birth")) as customer_age
                FROM "Customer" WHERE "Customer_ID"=1) as can_watch FROM "Movie" WHERE "Movie"."Name"='Schemers'

NUMBER 9.
        Get random seat on random seance
select seances.id as cst, seats.id as seat from seances, seats
where seats.hall_id=seances.hall_id order by random() limit 1

NUMBER 10.
        Get random ticket data
with rand_place as 
(select seances.id as rand_cast, seats.id as rand_seat from seances, seats where seats.hall_id=seances.hall_id order by random() limit 1)
select (select id from customers order by random() limit 1) as rand_dude,
rand_place.rand_cast, rand_place.rand_seat,
(select date(now() + interval '1 day' + random() * (interval '4 weeks'))) as rand_date from rand_place

NUMBER 11.
        Generate tickets (change counter in range to get necessary amount of tickets)
do $$
begin
for counter in 1..10 loop

insert into tickets (customer_id, seance_id, seat_id, airday)
with rand_place as 
(select seances.id as rand_cast, seats.id as rand_seat from seances, seats where seats.hall_id=seances.hall_id order by random() limit 1),
rand_date as
(select date(now() + interval '1 day' + random() * (interval '4 weeks')) as rand_day)
select (select id from customers order by random() limit 1) as rand_dude,
rand_place.rand_cast, rand_place.rand_seat,
rand_date.rand_day from rand_place, rand_date
where not exists
(select 1 from tickets as tick where tick.seat_id=rand_place.rand_seat and tick.airday=rand_date.rand_day and tick.seance_id=rand_place.rand_cast);

end loop;
end;
$$;

NUMBER 12
        Get up to 3 most viewed movies for specified age gap
select movies.name from movies where movies.id in
(select distinct movie_id from seances where seances.id in
(select seance_id from tickets where customer_id in
(select id from customers where date_part('year', age(dob::timestamp))<=40 and date_part('year', age(dob::timestamp))>=10)
group by seance_id
order by count(seance_id) desc
limit 3))

NUMBER 13
        Get all seats taken for specified seance and day
select row_num, string_agg(cast(seat_num as varchar), ', ' order by seat_num asc), seance_id
from tickets, seats
where tickets.seat_id=seats.id and tickets.airday='2020-11-08' and seance_id=118
group by seance_id, row_num

NUMBER 14
        Get amount of tickets sold for most popular time for specified movie
with max_tickets as
(select count(*) as tick_amt, seances.times_id as t1 from tickets
join seances on tickets.seance_id=seances.id
where seances.movie_id=1
group by seance_id, times_id order by count(*) desc)
select times.time, tick_amt from times join max_tickets on max_tickets.t1=times.id limit 1

NUMBER 15
        Count how many tickets were sold on specific time in general
select count(*), tm.time from tickets as t, seances as s, times as tm
where t.seance_id=s.id and s.times_id=tm.id
group by tm.time
order by 1 desc

NUMBER 16
        Get three most popular movies on specific date
select count(*), airday, movies.id, movies.name from tickets, seances, movies
where seances.id=tickets.seance_id and seances.movie_id=movies.id and airday='2020-11-09'
group by airday, movies.id, movies.name
order by 1 desc
limit 3

NUMBER 17 
        Get 10 VIP clients (Those who bought the most tickets)
select customers.name, customers.phone, count(*) as cnt from tickets
join customers on tickets.customer_id=customers.id
group by customers.name, customers.phone
order by cnt desc
limit 10

