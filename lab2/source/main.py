import CinemaModel as Model
import CinemaView as View
import CinemaController as Controller
import datetime

model = Model.CinemaModel()
view = View.CinemaView()
controller = Controller.CinemaController(model, view)
controller.start_work()

    #This part is responsible for regeneration of customers and tickets in database
    #It's left commented because it takes quite long to refill database with data each time
#view.intro()
#start = datetime.datetime.now()
#controller.intro_generator()
#finish =datetime.datetime.now()
#delta=(finish-start)
#view.show_time(delta.total_seconds()*1000)

view.menu()
command=""
while(command!="quit"):
    print("So, what's your next wish?")
    command = input()
    if(not command.isprintable() or len(command)==0):
        view.no_input()
    elif(command=="create/customer"):
        controller.create_customer()
    elif(command=="update/customer"):
        controller.update_customer()
    elif(command=="delete/customer"):
        controller.delete_customer()
    elif(command=="create/ticket"):
        controller.create_ticket()
    elif(command=="delete/ticket"):
        controller.delete_ticket()
    elif(command=='ein'):
        controller.calculate_first_statistics()
    elif(command=='zwei'):
        controller.calculate_second_statistics()
    elif(command=='drei'):
        controller.calculate_third_statistics()
    elif(command=="clear"):
        view.clear_screen()
    elif(command=="quit"):
        print("Bye-bye! Hope to see you again!")
    else:
        view.invalid_input(command)
controller.stop_work()
