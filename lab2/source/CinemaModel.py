import names
import psycopg2
import sys

class CinemaModel:
    def __init__(self, database_name = 'lab1', user = 'postgres', password = 'postgres', host = "localhost", port = "5432"):
        self.database_name = database_name
        self.user = user
        self.password = password
        self.host = host
        self.port = port

    def connect(self):
        try:
            self.connection = psycopg2.connect(
                dbname=self.database_name, user=self.user, password=self.password, host=self.host, port=self.port)
        except (Exception, psycopg2.Error) as error:
            self.connection = None
        if(self.is_connected()):
            self.cursor = self.connection.cursor()

    def disconnect(self):
        if(self.connection!=None):
            self.cursor.close()
            self.connection.close()

    def is_connected(self):
        return self.connection!=None

    def generate(self, customer_amount=500, tickers_amount=100000):
        self.cursor.execute('truncate customers cascade')
        self.generate_customers(customer_amount)
        self.generate_tickets(tickers_amount)

    def generate_customers(self, customer_amount=100):
        curr=0
        while curr<customer_amount:
            new_name=names.get_full_name()
            data=(new_name, )
            try:
                SQL='insert into customers (name, phone, dob) \
                    values (%s, cast(\'+38(0\' || \
                    cast(floor(random()*(100-50)+50) as varchar) || \')\' || \
                    cast(floor(random()*(1000-100)+100) as varchar)||\'-\'||\
                    cast(floor(random()*(100-50)+50) as varchar)||\'-\'||\
                    cast(floor(random()*(100-50)+50) as varchar) as varchar), \
                    date(timestamp \'1950-01-01 10:00:00\' + random() * (timestamp \'2010-01-10 10:00:00\' - \
                    timestamp \'1950-01-10 10:00:00\')))'
                value=(data, )
                self.cursor.execute(SQL, data)
                self.connection.commit()
                curr+=1
            except psycopg2.DatabaseError as error:
                pass

    def generate_tickets(self,tickets_amount=100000):
        curr=0
        while curr<tickets_amount:
            try:
                self.cursor.execute(
                    'insert into tickets (customer_id, seance_id, seat_id, airday)\
                    with rand_place as \
                    (select seances.id as rand_cast, seats.id as rand_seat from seances, \
                    seats where seats.hall_id=seances.hall_id order by random() limit 1),\
                    rand_date as\
                    (select date(now() + interval \'1 day\' + random() * (interval \'4 weeks\')) as rand_day)\
                    select (select id from customers order by random() limit 1) as rand_dude,\
                    rand_place.rand_cast, rand_place.rand_seat,\
                    rand_date.rand_day from rand_place, rand_date\
                    where not exists\
                    (select 1 from tickets as tick where tick.seat_id=rand_place.rand_seat \
                    and tick.airday=rand_date.rand_day and tick.seance_id=rand_place.rand_cast);')
                curr=curr+1
            except psycopg2.DatabaseError as error:
                pass
        self.connection.commit()

    def create_customer(self, name, phone, DOB):
        try:
            SQL='insert into customers (name, phone, dob) \
                values (%s, %s, TO_DATE(%s, \'YYYY-MM-DD\')) returning id'
            data=(name, phone, DOB)
            self.cursor.execute(SQL, data)
            self.connection.commit()
            return self.cursor.fetchone()
        except:
            self.connection.rollback()
            return -1

    def update_customer(self, ID, name, phone, DOB):
        try:
            SQL='update customers set name=%s, phone=%s, dob=to_date(%s, \'YYYY-MM-DD\')\
                where id=cast(%s as integer)'
            data=(name, phone, DOB, int(ID))
            self.cursor.execute(SQL, data)
            self.connection.commit()
            return True
        except:
            self.connection.rollback()
            return False

    def delete_customer(self, ID):
        try:
            self.cursor.execute('delete from tickets where customer_id={}'.format(int(ID)))
            self.cursor.execute('delete from customers where id = {}'.format(int(ID)))
            self.connection.commit()
            return True
        except:
            self.connection.rollback()
            return False

    def find_customer(self, data):
        try:
            if(data.isdigit()):
                self.cursor.execute('select * from customers where id={}'.format(int(data)))
            else:
                SQL='select * from customers where phone=%s'
                value=(data, )
                self.cursor.execute(SQL, value)
            return self.cursor.fetchone()
        except:
            return -1

    def check_customer_dependencies(self, data):
        customer=self.find_customer(data)
        if(not customer==None):
            try:
                self.cursor.execute('select * from tickets where customer_id={} limit 1'.format(data[0]))
                return self.cursor.fetchone()
            except:
                return -1
        else:
            return customer

    def get_movies(self, customer_id):
        try:
            self.cursor.execute('select id, name, length, age_restriction from movies where age_restriction<\
            (select date_part(\'year\', now()) - date_part(\'year\', dob) from customers where id={})'
            .format(int(customer_id)))
            return self.cursor.fetchall()
        except:
            return -1

    def get_seances(self, movie_id):
        try:
            self.cursor.execute('select seances.id, hall_id, times.id, times.time \
                from seances join times on seances.times_id=times.id where movie_id={} \
                order by times.time, hall_id'.format(movie_id))
            return self.cursor.fetchall()
        except:
            return -1

    def get_taken_seats(self, airday, seance_id):
        try:
            SQL='select row_num, string_agg(cast(seat_num as varchar), \', \' order by seat_num asc)\
                from tickets, seats where tickets.seat_id=seats.id and tickets.airday=to_date(%s, \'yyyy-mm-dd\')\
                and seance_id=%s group by seance_id, row_num'
            data=(airday, int(seance_id))
            self.cursor.execute(SQL, data)
            return self.cursor.fetchall()
        except:
            return -1

    def get_row_amount(self, hall_id):
        try:
            self.cursor.execute('select seat_amount/15 from halls where id={}'.format(hall_id))
            return self.cursor.fetchone()
        except:
            return -1

    def create_ticket(self, customer_id, seance_id, seat_row, seat_col, airday):
        try:
            SQL='insert into tickets (customer_id, seance_id, airday, seat_id) \
                values (%s, %s, TO_DATE(%s, \'YYYY-MM-DD\'), \
                (select id from seats where row_num=%s and seat_num=%s and \
                hall_id=(select hall_id from seances where id=%s))) returning id'
            data=(int(customer_id), int(seance_id), airday, int(seat_row), int(seat_col), int(seance_id))
            self.cursor.execute(SQL, data)
            self.connection.commit()
            return self.cursor.fetchone()
        except:
            self.connection.rollback()
            return -1

    def find_ticket(self, data):
        try:
            self.cursor.execute('select * from tickets where id={}'.format(int(data)))
            return self.cursor.fetchone()
        except:
            return -1

    def delete_ticket(self, ID):
        try:
            self.cursor.execute('delete from tickets where id={}'.format(int(ID)))
            self.connection.commit()
            return True
        except psycopg2.Error as m:
            print(m)
            self.connection.rollback()
            return False

    def get_first_statistics(self, age_from, age_to):
        try:
            self.cursor.execute('select movies.name from movies where movies.id in\
                (select distinct movie_id from seances where seances.id in\
                (select seance_id from tickets where customer_id in\
                (select id from customers where date_part(\'year\', age(dob::timestamp))<={} \
                and date_part(\'year\', age(dob::timestamp))>={})\
                group by seance_id order by count(seance_id) desc limit 3))'
                .format(int(age_to), int(age_from)))
            return self.cursor.fetchall()
        except:
            return -1

    def get_second_statistics(self, movie_id):
        try:
            self.cursor.execute('with max_tickets as \
                (select count(*) as tick_amt, seances.times_id as t1 from tickets \
                join seances on tickets.seance_id=seances.id \
                where seances.movie_id={} \
                group by seance_id, times_id order by count(*) desc) \
                select times.time, tick_amt from times \
                join max_tickets on max_tickets.t1=times.id limit 1'.format(int(movie_id)))
            return self.cursor.fetchone()
        except:
            return -1

    def get_third_statistics(self, amt):
        try:
            self.cursor.execute('select customers.name, customers.phone, count(*) as cnt from tickets\
                join customers on tickets.customer_id=customers.id\
                group by customers.name, customers.phone order by cnt desc limit {}'.format(int(amt)))
            return self.cursor.fetchall()
        except:
            return -1

    def get_all_movies(self):
        try:
            self.cursor.execute('select * from movies')
            return self.cursor.fetchall()
        except:
            return -1
