from ClassBase import Base
from sqlalchemy import CheckConstraint, Column, Integer, VARCHAR

class Hall(Base):
    __tablename__ = 'halls'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    name = Column('name', VARCHAR(255), nullable=False, unique=True)
    seat_amount = Column('seat_amount', Integer, nullable=False)
    floor = Column('floor', Integer, nullable=False)
    CheckConstraint('seat_amount > 0')
    CheckConstraint('floor >= 0')

    def __init__(self, name, seat_amount, floor):
        self.name=name
        self.seat_amount=seat_amount
        self.floor=floor