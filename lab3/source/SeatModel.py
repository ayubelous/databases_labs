from ClassBase import Base
from sqlalchemy import CheckConstraint, Column, ForeignKey, Integer, UniqueConstraint

class Seat(Base):
    __tablename__ = 'seats'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    hall_id = Column(Integer, ForeignKey('halls.id', onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    row_num = Column('row_num', Integer, nullable=False)
    seat_num = Column('seat_num', Integer, nullable=False)
    UniqueConstraint('hall_id', 'row_num', 'seat_num')
    CheckConstraint('seat_num<=15')

    def __init__(self, hall_id, row_num, seat_num):
        self.hall_id=hall_id
        self.row_num=row_num
        self.seat_num=seat_num