from ClassBase import Base
from sqlalchemy import Column, ForeignKey, Integer

class Seance(Base):
    __tablename__ = 'seances'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    hall_id = Column(Integer, ForeignKey('halls.id', onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    movie_id = Column(Integer, ForeignKey('movies.id', onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    times_id = Column(Integer, ForeignKey('times.id', onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)

    def __init__(self, hall_id, movie_id, times_id):
        self.hall_id=hall_id
        self.movie_id=movie_id
        self.times_id=times_id