from ClassBase import Base
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import CheckConstraint, Column, DATE, Integer, ForeignKey, UniqueConstraint

class Ticket(Base):
    __tablename__ = 'tickets'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    seat_id = Column(Integer, ForeignKey('seats.id', onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    customer_id = Column(Integer, ForeignKey('customers.id', onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    airday = Column('airday', DATE, nullable=False)
    seance_id = Column(Integer, ForeignKey('seances.id', onupdate='RESTRICT', ondelete='RESTRICT'), nullable=False)
    UniqueConstraint('seat_id', 'seance_id', 'airday')
    CheckConstraint('airday > CURRENT_DATE')

    def __init__(self, seat_id, customer_id, airday, seance_id):
        self.seat_id=seat_id
        self.customer_id=customer_id
        self.airday=airday
        self.seance_id=seance_id