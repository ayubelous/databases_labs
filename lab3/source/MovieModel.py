from ClassBase import Base
from sqlalchemy import CheckConstraint, Column, Integer, VARCHAR

class Movie(Base):
    __tablename__ = 'movies'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    name = Column('name', VARCHAR(255), nullable=False, unique=True)
    length = Column('length', Integer, nullable=False)
    age_restriction = Column('age_restriction', Integer, nullable=False)
    CheckConstraint('length > 0')
    CheckConstraint('age_restriction >= 0')

    def __init__(self, name, length, age_restriction):
        self.name=name
        self.length=length
        self.age_restriction=age_restriction