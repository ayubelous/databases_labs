from ClassBase import Base
from sqlalchemy import CheckConstraint, Column, DATE, Integer, VARCHAR

class Customer(Base):
    __tablename__ = 'customers'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    name = Column('name', VARCHAR(255), nullable=False)
    dob = Column('dob', DATE, nullable=False)
    phone = Column('phone', VARCHAR(255), nullable=False, unique=True)
    CheckConstraint('phone::text ~ \'\+38\(0\d\d\)\d\d\d\-\d\d\-\d\d\'::text')

    def __init__(self, name, dob, phone):
        self.name=name
        self.dob=dob
        self.phone=phone
