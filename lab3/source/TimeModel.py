from ClassBase import Base
from sqlalchemy import Column, Integer, TIME

class Time(Base):
    __tablename__ = 'times'
    id = Column(Integer, primary_key=True, autoincrement=True, nullable=False, unique=True)
    time = Column('time', TIME, nullable=False)

    def __init__(self, time):
        self.time=time