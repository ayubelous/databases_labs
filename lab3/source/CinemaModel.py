import names
import psycopg2
import sqlalchemy as sql
import sys

from CustomerModel import Customer
from HallModel import Hall
from MovieModel import Movie
from SeanceModel import Seance
from SeatModel import Seat
from TicketModel import Ticket
from TimeModel import Time

from sqlalchemy import and_, cast, create_engine, DATE, delete, exc, update, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.sql import extract, func, text
from sqlalchemy.sql.elements import Label

class CinemaModel:
    def __init__(self, database_name = 'lab1', user = 'postgres', password = 'postgres', host = "localhost", port = "5432"):
        self.database_name = database_name
        self.user = user
        self.password = password
        self.host = host
        self.port = port

    def connect(self):
            self.engine=create_engine('postgres+psycopg2://{}:{}@{}:{}/{}'
                .format(self.user, self.password, self.host, self.port, self.database_name))
            Session = sessionmaker(bind=self.engine)
            self.session=Session()

    def disconnect(self):
        if(self.is_connected()):
            self.session.close()

    def is_connected(self):
        return self.session!=None

    def generate(self, customer_amount=500, tickets_amount=100000):
        self.session.execute('truncate customers cascade')
        self.generate_customers(customer_amount)
        self.generate_tickets(tickets_amount)

    def generate_customers(self, customer_amount=100):
        curr=0
        while curr<customer_amount:
            new_name=names.get_full_name()
            data=({"new_name": new_name})
            try:
                SQL="""insert into customers (name, phone, dob) \
                    values (:new_name, cast('+38(0' || \
                    cast(floor(random()*(100-50)+50) as varchar) || ')' || \
                    cast(floor(random()*(1000-100)+100) as varchar)||'-'||\
                    cast(floor(random()*(100-50)+50) as varchar)||'-'||\
                    cast(floor(random()*(100-50)+50) as varchar) as varchar), \
                    date(timestamp '1950-01-01 10:00:00' + random() * (timestamp '2010-01-10 10:00:00' - \
                    timestamp '1950-01-10 10:00:00')))"""
                self.session.execute(SQL, data)
                self.session.commit()
                curr+=1
            except(Exception) as err:
                print(err)
                return

    def generate_tickets(self,tickets_amount=100000):
        curr=0
        while curr<tickets_amount:
            try:
                self.session.execute(
                    """insert into tickets (customer_id, seance_id, seat_id, airday)\
                    with rand_place as \
                    (select seances.id as rand_cast, seats.id as rand_seat from seances, \
                    seats where seats.hall_id=seances.hall_id order by random() limit 1),\
                    rand_date as\
                    (select date(now() + interval '1 day' + random() * (interval '4 weeks')) as rand_day)\
                    select (select id from customers order by random() limit 1) as rand_dude,\
                    rand_place.rand_cast, rand_place.rand_seat,\
                    rand_date.rand_day from rand_place, rand_date\
                    where not exists\
                    (select 1 from tickets as tick where tick.seat_id=rand_place.rand_seat \
                    and tick.airday=rand_date.rand_day and tick.seance_id=rand_place.rand_cast);""")
                print("Added ticket # {}".format(curr))
                curr=curr+1
            except(Exception) as err:
                print(err)
                return
        self.session.commit()

    def create_customer(self, name, phone, DOB):
        try:
            new_cust = Customer(name, DOB, phone)
            self.session.add(new_cust)
            self.session.commit()
            return new_cust.id
        except:
            self.session.rollback()
            return -1

    def update_customer(self, ID, name, phone, DOB):
        try:
            to_update=self.find_customer(ID)
            to_update.name=name
            to_update.dob=DOB
            to_update.phone=phone
            self.session.commit()
            return True
        except:
            self.session.rollback()
            return False

    def delete_customer(self, ID):
        try:
            self.session.query(Ticket).filter(Ticket.customer_id==ID).delete()
            self.session.query(Customer).filter(Customer.id==ID).delete()
            self.session.commit()
            return True
        except:
            self.session.rollback()
            return False

    def find_customer(self, data):
        try:
            if(type(data) is int or data.isdigit()):
                return self.session.query(Customer).filter(Customer.id==data).first()
            else:
                return self.session.query(Customer).filter(Customer.phone==data).first()
        except:
            return -1

    def check_customer_dependencies(self, data):
        customer=self.find_customer(data.id)
        if(not customer==None):
            try:
                return self.session.query(Ticket).filter(Ticket.customer_id==data.id).count()
            except:
                return -1
        else:
            return customer

    def get_movies(self, customer_id):
        try:
            age=self.session.query(extract('year', func.age(Customer.dob))).filter(Customer.id==customer_id).first()
            return self.session.query(Movie).filter(Movie.age_restriction<age).all()
        except:
            return -1

    def get_seances(self, movie_id):
        try:
            time_id=Label("time_id", Time.id)
            return self.session.query(Seance.id, Seance.hall_id, time_id, Time.time)\
                .filter(and_(Seance.times_id==Time.id, Seance.movie_id==movie_id)).all()
        except:
            return -1

#I couldn't find a proper way to remake this statement using sqlAlchemy, so I just used raw SQL
    def get_taken_seats(self, airday, seance_id):
        try:
            return self.session.execute("""select row_num, string_agg(cast(seat_num as varchar), ', ' order by seat_num asc)\
                from tickets, seats where tickets.seat_id=seats.id and tickets.airday=to_date(:airday, 'yyyy-mm-dd')\
                and seance_id=:seance_id group by seance_id, row_num""", {"airday": airday, "seance_id": seance_id}).fetchall()
        except:
            return -1

    def get_row_amount(self, hall_id):
        try:
            seat_amt=Label("seat_amt", Hall.seat_amount/15)
            return self.session.query(seat_amt).filter(Hall.id==hall_id).first()
        except:
            return -1

    def create_ticket(self, customer_id, seance_id, seat_row, seat_col, airday):
        try:
            hall_id=self.session.query(Seance.hall_id).filter(Seance.id==seance_id).first()
            seat_id=self.session.query(Seat.id).\
                filter(and_(Seat.row_num==seat_row, Seat.seat_num==seat_col, Seat.hall_id==hall_id))
            new_tick=Ticket(seat_id, customer_id, cast(airday, DATE), seance_id)
            self.session.add(new_tick)
            self.session.commit()
            return new_tick.id
        except:
            self.session.rollback()
            return -1

    def find_ticket(self, data):
        try:
            return self.session.query(Ticket).filter(Ticket.id==data).first()
        except:
            return -1

    def delete_ticket(self, ID):
        try:
            self.session.query(Ticket).filter(Ticket.id==ID).delete()
            self.session.commit()
            return True
        except:
            self.connection.rollback()
            return False

    def get_all_movies(self):
        try:
            return self.session.query(Movie).all()
        except:
            return -1

#Since these three methods are optional analytical information, I decided to keep it as raw SQL
    def get_first_statistics(self, age_from, age_to):
        try:
            return self.session.execute("""select movies.name from movies where movies.id in\
                (select distinct movie_id from seances where seances.id in\
                (select seance_id from tickets where customer_id in\
                (select id from customers where date_part('year', age(dob::timestamp))<=:age_to \
                and date_part('year', age(dob::timestamp))>=:age_from)\
                group by seance_id order by count(seance_id) desc limit 3))""",
                {"age_to": age_to, "age_from": age_from}).fetchall()
        except:
            return -1

    def get_second_statistics(self, movie_id):
        try:
            return self.session.execute("""with max_tickets as \
                (select count(*) as tick_amt, seances.times_id as t1 from tickets \
                join seances on tickets.seance_id=seances.id \
                where seances.movie_id=:movie_id \
                group by seance_id, times_id order by count(*) desc) \
                select times.time, tick_amt from times \
                join max_tickets on max_tickets.t1=times.id limit 1""", {"movie_id": movie_id}).fetchone()
        except:
            return -1

    def get_third_statistics(self, amt):
        try:
            return self.session.execute("""select customers.name, customers.phone, count(*) as cnt from tickets\
                join customers on tickets.customer_id=customers.id\
                group by customers.name, customers.phone order by cnt desc limit :amt""", {"amt": amt}).fetchall()
        except:
            return -1
