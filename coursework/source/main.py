import HospitalModel as Model
import HospitalView as View
import HospitalController as Controller

model = Model.HospitalModel()
view = View.HospitalView()
controller = Controller.HospitalController(model, view)
controller.start_work()

view.menu()
command=""
while(command!='pass'):
    print("So, what's your next wish?")
    command = input()
    if(not command.isprintable() or len(command)==0):
        view.no_input()
    elif(command=='generate'): #Warning: executing this command might take several minutes for big data amount
        controller.intro_generator() #All generated data is automatically exported in ./data folder in specified json files
    elif(command=='import'):
        controller.import_data()
    elif(command=='export'):
        controller.export_data()
    elif(command=='ein'):
        controller.calculate_first_statistics()
    elif(command=='zwei'):
        controller.calculate_second_statistics()
    elif(command=='drei'):
        controller.calculate_third_statistics()
    elif(command=='find'):
        controller.find_patient()
    elif(command=='measure'):
        controller.find_measurements()
    elif(command=='clean'):
        view.clear_screen()
    elif(command=='pass'):
        print("Bye-bye! Drop by for another drink!")
    else:
        print("Sorry, we haven't been taught how to cope with {}. Try something else".format(command))
controller.stop_work()
