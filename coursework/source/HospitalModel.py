import json
import names
import os
import random
import sqlalchemy
import sys

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

class HospitalModel:
    def __init__(self, database_name = 'coursework', user = 'postgres', password = 'postgres',
            host = "localhost", port = "5432",
            backup_file=os.getcwd()+"/data/hospital.dump",
            patient_file=os.getcwd()+"/data/export_patients.json",
            measurement_file=os.getcwd()+"/data/export_measurements.json"):
        self.database_name = database_name
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.backup_file=backup_file
        self.patient_file=patient_file
        self.measurement_file=measurement_file

    def connect(self):
        self.engine=create_engine('postgres+psycopg2://{}:{}@{}:{}/{}'
            .format(self.user, self.password, self.host, self.port, self.database_name))
        Session = sessionmaker(bind=self.engine)
        self.session=Session()

    def disconnect(self):
        if(self.is_connected()):
            self.session.close()

    def is_connected(self):
        return self.session!=None

    def generate(self, patients_amount=3000, measurements_amount=100000):
        self.session.execute('truncate patients cascade')
        self.generate_patients(patients_amount)
        self.generate_measurements(measurements_amount)
        self.export_patients_json()
        self.export_measurements_json()

    def generate_patients(self, patients_amount):
        curr=0
        while int(self.session.execute("select count(*) as curr from patients").fetchone().curr)<patients_amount:
            new_name=names.get_first_name()
            new_surname=names.get_last_name()
            data=({"new_name": new_name, "new_surname": new_surname})
            try:
                SQL="""select * from insert_patient(:new_name, :new_surname)"""
                self.session.execute(SQL, data)
                curr+=1
                print("Added patient №{}".format(curr))
            except(Exception) as err:
                print(err)
                pass
        self.session.commit()

    def generate_measurements(self, measurements_amount):
        self.session.execute("""select * from insert_measurements({})""".format(int(measurements_amount)))
        self.session.commit()

    def export_data(self):
        command_str = "/usr/lib/postgresql12/bin/pg_dump -h {} -d {} -p {} -U {} -F c > {}\
        ".format(self.host, self.database_name, self.port, self.user, self.backup_file)
        os.system(command_str)

    def import_data(self):
        command_str = "/usr/lib/postgresql12/bin/pg_restore -h {} -d {} -p {} -U {} -c {}\
            ".format(self.host, self.database_name, self.port, self.user, self.backup_file)
        os.system(command_str)

    def export_patients_json(self):
        patients=self.session.execute("select json_agg(pats) as result from (select * from patients) as pats").fetchone()
        with open(self.patient_file, 'w') as out_file:
            json.dump(patients.result, out_file)

    def export_measurements_json(self):
        ments=self.session.execute("select json_agg(mes) as result from (select * from measurements) as mes").fetchone()
        with open(self.measurement_file, 'w') as out_file:
            json.dump(ments.result, out_file)

    def get_first_statistics(self, does_smoke, does_drink):
        try:
            return self.session.execute("select count(*) as amt, activity_pulse-peace_pulse as pulse_diff\
                from measurements ms join patients pt on pt.id=ms.patient_id\
                where pt.is_smoking={}::bool and pt.is_drinking={}::bool\
                group by pt.is_smoking, pt.is_drinking, pulse_diff\
                order by is_smoking, is_drinking, pulse_diff desc".format(int(does_smoke), int(does_drink)))
        except Exception as err:
            print(err)
            self.session.rollback()
            return -1

    def get_second_statistics(self, patients_amount):
        try:
            return self.session.execute("select distinct pt.id, pt.name, pt.surname, pt.phone,\
                activity_systolic_pressure-peace_systolic_pressure as syst_diff,\
                activity_diastolic_pressure-peace_diastolic_pressure as diast_diff\
                from measurements ms join patients pt on pt.id=ms.patient_id\
                where pt.has_diabetes=true order by syst_diff desc, diast_diff desc\
                limit {}".format(patients_amount)).fetchall()
        except Exception as err:
            print(err)
            self.session.rollback()
            return -1

    def get_third_statistics_first(self, is_sedentary):
        try:
            return self.session.execute("select count(*) as amt,\
                activity_systolic_pressure-peace_systolic_pressure as syst_diff\
                from measurements ms join patients pt on pt.id=ms.patient_id\
                where pt.is_sedentary={}::bool\
                group by syst_diff order by syst_diff desc".format(int(is_sedentary))).fetchall()
        except Exception as err:
            print(err)
            self.session.rollback()
            return -1

    def get_third_statistics_second(self, is_sedentary):
        try:
            return self.session.execute("select count(*) as amt,\
                activity_diastolic_pressure-peace_diastolic_pressure as diast_diff\
                from measurements ms join patients pt on pt.id=ms.patient_id\
                where pt.is_sedentary={}::bool\
                group by diast_diff order by diast_diff desc".format(int(is_sedentary))).fetchall()
        except Exception as err:
            print(err)
            self.session.rollback()
            return -1

    def get_patient_measurements(self, patient_phone):
        try:
            return self.session.execute('select ms.measurement_date, ms.peace_pulse, ms.activity_pulse,\
            ms.peace_systolic_pressure, ms.activity_systolic_pressure,\
                ms.peace_diastolic_pressure, ms.activity_diastolic_pressure\
                from measurements ms join patients pt on ms.patient_id=pt.id where pt.phone=:phone\
                order by measurement_date', {'phone': patient_phone}).fetchall()
        except Exception as err:
            print(err)
            self.session.rollback()
            return -1

    def get_patient_data(self, patient_surname):
        try:
            return self.session.execute('select id, name, surname, phone from patients where surname like :surname',
                {'surname': patient_surname+'%'}).fetchall()
        except Exception as err:
            print(err)
            self.session.rollback()
            return -1
