import matplotlib.pyplot as plt
import numpy
import pandas
from os import system

class HospitalView:
    def intro(self):
        print("Now we will generate some new patients and get their measurements")
        print("Don't worry: there will be no leftovers from previous users")
        print("Only your personal patients and their measurements")

    def menu(self):
        print("\t\tGreetings, my lovely friend. Welcome to our hospital")
        print("\tDon't be afraid: have a seat. Tea? Coffee? Statistics?")
        print("Say generate to generate new data to our database")
        print("Say export to create a dump of current database version")
        print("Say import to restore database from last database dump")
        print("Say find to find patient(s) with specified surname and their info")
        print("Say measure to get the measurements of specified patient")
        print("Say ein to see the pulse dependency on whether patient smokes and/or drinks")
        print("Say zwei to get the list of patients with diabetes that have pressure problems")
        print("Say drei to see the pressure dependency on whether patient exercises regularly or not")
        print("Say clean to clean up the screen and start our conversation anew")
        print("Say pass to go home and have a rest after a hard day")
        print("\tWarning: generation might take several minutes if the data quantity is too high")

    def clear_screen(self):
        system('clear')
        self.menu()

    def no_input(self):
        print("Don't be shy: speak up, doctor's listening closely")

    def no_number(self):
        print("That doesn't seem like an appropriate number. Try again")

    def print_first_stats(self, pulse_deltas, pats_amt, does_smoke, does_drink):
        fig = plt.gcf()
        plt.plot(pulse_deltas, pats_amt, color='red', linewidth=1)
        plt.suptitle('Pulse difference analysis\nPatients are smokers: {}\nPatients are drinkers: {}\
            '.format(bool(int(does_smoke)), bool(int(does_drink))))
        plt.xlabel('Delta between pulse before and after exercizing, BPM')
        plt.ylabel('Amount of patients with this delta')
        plt.xticks(numpy.arange(min(pulse_deltas), max(pulse_deltas), 5))
        plt.yticks(numpy.arange(min(pats_amt), max(pats_amt), 10))
        plt.show()
        return fig

    def print_second_stats(self, people):
        print("\t\tHere are {} people that should be taken care of:".format(len(people)))
        for person in people:
            print("\tPatient № " + str(person.id))
            print("Name: " + person.name)
            print("Surname: " + person.surname)
            print("Phone: " + person.phone)
            print("Systolic pulse delta: " + str(person.syst_diff))
            print("Diastolic pulse delta: " + str(person.diast_diff)+ "\n")

    def print_third_stats(self, syst_deltas, syst_pats_amt, diast_deltas, diast_pats_amt, is_sedentary):
        plt.subplot(2, 1, 1)
        plt.plot(syst_deltas, syst_pats_amt, color='red', linewidth=1)
        plt.xlabel('Delta between systolic pressure before and after exercizing, BPM')
        plt.ylabel('Amount of patients with this delta')
        plt.subplot(2, 1, 2)
        plt.plot(diast_deltas, diast_pats_amt, color='blue', linewidth=1)
        plt.xlabel('Delta between diastolic pressure before and after exercizing, BPM')
        plt.ylabel('Amount of patients with this delta')
        plt.suptitle('Pressure difference analysis\nPatients have sedentary lifestyle: {}'.format(bool(int(is_sedentary))))
        fig = plt.gcf()
        plt.show()
        return fig

    def print_measurements(self, measurements):
        col_labels=["Date", "Pulse (P)", "Pulse (A)", "Syst pressure (P)", "Syst pressure (A)",
            "Diast pressure (P)", "Diast pressure (A)"]
        bar_dataframe = pandas.DataFrame(measurements, columns=col_labels)
        bar_plot=bar_dataframe.plot(x="Date", y=col_labels[1:9], kind="bar", align='center', width=0.8, linewidth=1)
        plt.ylabel('Measurement values')
        plt.xticks(rotation=45)
        fig = plt.gcf()
        plt.show()
        return fig

    def print_patient_data(self, people):
        print("\t\tHere are all the matching cases:")
        for person in people:
            print("\tPatient № " + str(person.id))
            print("Name: " + person.name)
            print("Surname: " + person.surname)
            print("Phone: " + person.phone)

    def show_time(self, time):
        print("And the query took only {} milliseconds to do!".format(time))