import datetime
import HospitalModel as Model
import HospitalView as View
import matplotlib.pyplot as plt
import numpy
import pandas
import re

class HospitalController:
    def __init__(self, model, view):
        self.model=model
        self.view=view
        self.save_folder='./data/'

    def __del__(self):
        if(self.model.is_connected()):
            self.model.disconnect()

    def start_work(self):
        self.model.connect()

    def stop_work(self):
        self.model.disconnect()

    def intro_generator(self):
        self.view.intro()
        patient_amt, measurement_amt=("","")
        while(True):
            print("So, how many patients will come in next two months?")
            patient_amt=input()
            if(not patient_amt.isprintable() or len(patient_amt)==0):
                self.view.no_input()
            elif(patient_amt.isdigit()==True):
                break
            else:
                self.view.no_number()
        while(True):
            print("And how many measurements will hospital have done in next two months?")
            measurement_amt=input()
            if(not measurement_amt.isprintable() or len(measurement_amt)==0):
                self.view.no_input()
            elif(measurement_amt.isdigit()==True):
                break
            else:
                self.view.no_number()
        start = datetime.datetime.now()
        self.model.generate(int(patient_amt), int(measurement_amt))
        finish = datetime.datetime.now()
        delta=(finish-start)
        self.view.show_time(delta.total_seconds()*1000)

    def import_data(self):
        start = datetime.datetime.now()
        self.model.import_data()
        finish = datetime.datetime.now()
        delta=(finish-start)
        self.view.show_time(delta.total_seconds()*1000)

    def export_data(self):
        start = datetime.datetime.now()
        self.model.export_data()
        finish = datetime.datetime.now()
        delta=(finish-start)
        self.view.show_time(delta.total_seconds()*1000)

    def save_graph(self, fig):
        print("Would you like to save the graph you've seen? Enter 1 if you want it or anything else if you don't")
        to_save=input()
        if(to_save=="1"):
            fig.set_size_inches((15, 11), forward=False)
            fig.savefig(self.save_folder+"pulse_plot-"+str(int(datetime.datetime.now().timestamp()))+".png", dpi=250)
            print("Graph saved to {} folder".format(self.save_folder))
        else:
            print("Okay. Graph not saved")

    def calculate_first_statistics(self):
        does_drink=""
        does_smoke=""
        while(True):
            print("So, do our subjects smoke? Enter 0 for no and 1 for yes")
            does_smoke=input()
            if(not does_smoke.isdigit()):
                self.view.no_number()
            elif(len(does_smoke)==0):
                self.view.no_input()
            elif(int(does_smoke)!=0 and int(does_smoke)!=1):
                print("Is it too hard for you to choose from two numbers? I'll give you one more try")
            else:
                break
        while(True):
            print("Now, do our subjects drink? Enter 0 for no and 1 for yes")
            does_drink=input()
            if(not does_drink.isdigit()):
                self.view.no_number()
            elif(len(does_drink)==0):
                self.view.no_input()
            elif(int(does_drink)!=0 and int(does_drink)!=1):
                print("Is it too hard for you to choose from two numbers? I'll give you one more try")
            else:
                break
        start=datetime.datetime.now()
        patients=self.model.get_first_statistics(does_smoke, does_drink)
        finish=datetime.datetime.now()
        delta=(finish-start)
        self.view.show_time(delta.total_seconds()*1000)
        pulse_deltas=[]
        pats_amt=[]
        for pt in patients:
            pulse_deltas.append(int(pt.pulse_diff))
            pats_amt.append(int(pt.amt))
        self.save_graph(self.view.print_first_stats(pulse_deltas, pats_amt, does_smoke, does_drink))

    def calculate_second_statistics(self):
        patients_amount=0
        print("So, how many people with diabetes do you want to help today?")
        while(True):
            patients_amount=input()
            if(not patients_amount.isprintable() or len(patients_amount)==0):
                self.view.no_input()
            elif(patients_amount.isdigit()==True):
                break
            else:
                self.view.no_number()
        start=datetime.datetime.now()
        result=self.model.get_second_statistics(patients_amount)
        finish=datetime.datetime.now()
        delta=(finish-start)
        self.view.print_second_stats(result)
        self.view.show_time(delta.total_seconds()*1000)

    def calculate_third_statistics(self):
        while(True):
            print("One simple question: do our patients have a sedentary lifestyle? Enter 0 for no and 1 for yes")
            is_sedentary=input()
            if(not is_sedentary.isdigit()):
                self.view.no_number()
            elif(len(is_sedentary)==0):
                self.view.no_input()
            elif(int(is_sedentary)!=0 and int(is_sedentary)!=1):
                print("Is it too hard for you to choose from two numbers? I'll give you one more try")
            else:
                break
        start=datetime.datetime.now()
        syst_data=self.model.get_third_statistics_first(is_sedentary)
        diast_data=self.model.get_third_statistics_second(is_sedentary)
        finish=datetime.datetime.now()
        delta=(finish-start)
        self.view.show_time(delta.total_seconds()*1000)
        syst_deltas=[]
        diast_deltas=[]
        syst_pats_amt=[]
        diast_pats_amt=[]
        for pt in syst_data:
            syst_deltas.append(int(pt.syst_diff))
            syst_pats_amt.append(int(pt.amt))
        for pt in diast_data:
            diast_deltas.append(int(pt.diast_diff))
            diast_pats_amt.append(int(pt.amt))
        self.save_graph(self.view.print_third_stats(syst_deltas, syst_pats_amt, diast_deltas, diast_pats_amt, is_sedentary))

    def find_measurements(self):
        phone_format=re.compile("^\+38\(0[0-9][0-9]\)[0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$")
        print("Enter patient's phone number")
        data=input()
        measurements=[]
        if(phone_format.match(data)):
            measurements=self.model.get_patient_measurements(data)
            if(len(measurements)>0):
                self.save_graph(self.view.print_measurements(measurements))
            else:
                print("It appears that this patient haven't done any measurements in our clinic yet...")
        else:
            print("That's not an valid phone. We're done here")

    def find_patient(self):
        print("Enter patient's surname")
        data=input()
        patients=[]
        if(len(data)>0):
            patients=self.model.get_patient_data(data)
            if(len(patients)>0):
                self.view.print_patient_data(patients)
            else:
                print("It appears that there are no patients with this surname in our clinic yet...")
        else:
            print("I can't hear you, so we'll end here")
